import React, { useState } from "react";
import { View, Text, Card, CardItem, Body, Icon, Button } from "native-base";

const InfoBox = props => {
  const [infoBoxVisible, setInfoBoxVisible] = useState(true);

  const handleOkButton = () => {
    setInfoBoxVisible(false);
  };

  let infoBoxCard = (
    <Card>
      <CardItem header>
        <Icon name="information-circle-outline" style={{ color: "#fff" }} />
        <Text body-bold-dark>{props.title}</Text>
      </CardItem>
      <CardItem>
        <Body>
          <Text label-dark>{props.message}</Text>
        </Body>
      </CardItem>
      <CardItem>
        <Body>
          <Button full button-link-dark onPress={() => handleOkButton()}>
            <Text>Close</Text>
          </Button>
        </Body>
      </CardItem>
    </Card>
  );

  if (!infoBoxVisible) {
    infoBoxCard = null;
  }

  return <View>{infoBoxCard}</View>;
};

export default InfoBox;
