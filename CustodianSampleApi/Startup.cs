﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CustodianSampleApi.Data;
using CustodianSampleApi.Logic;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Models;
using CustodianSampleApi.Repo;
using CustodianSampleApi.Services;
using Microsoft.OpenApi.Models;

namespace CustodianSampleApi
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            Environment = environment;
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSwaggerGen();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("DefaultConnection")));

            services.AddHttpClient("HorizonClient");
            services.AddHttpClient("NexusApiClient", c =>
            {
                c.BaseAddress = new Uri(Configuration.GetSection("Nexus").GetValue<string>("nexus-api-url"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            var builder = services.AddIdentityServer(options =>
                {
                    options.Events.RaiseErrorEvents = true;
                    options.Events.RaiseInformationEvents = true;
                    options.Events.RaiseFailureEvents = true;
                    options.Events.RaiseSuccessEvents = true;
                })
                .AddInMemoryIdentityResources(Config.Ids)
                .AddInMemoryApiResources(Config.Apis)
                .AddInMemoryClients(Config.Clients)
                .AddAspNetIdentity<ApplicationUser>();

            services.AddScoped<ITransactionLogic, TransactionLogic>();
            services.AddSingleton<INexusApiService, NexusApiService>();

            services.AddScoped<ICustomerRepo, CustomerRepo>();
            services.AddScoped<ICustomerLogic, CustomerLogic>();

            services.AddScoped<IBalanceLogic, BalanceLogic>();
            services.AddScoped<IPriceLogic, PriceLogic>();

            // not recommended for production - you need to store your key material somewhere secure
            builder.AddDeveloperSigningCredential();

            // Needed because IdentityServer and API are in 1 project
            services.AddLocalApiAuthentication();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Nexus Custodian Proxy", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDatabaseErrorPage();
                app.UseExceptionHandler("/error-local-development");
            }
            else
            {
                app.UseHttpsRedirection();
                app.UseExceptionHandler("/error");
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseIdentityServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}