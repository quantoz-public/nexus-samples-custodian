import React, { useState, useEffect } from "react";
import {
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Container,
  Content
} from "native-base";
import { SafeAreaView } from "react-native";
import { getBalance, getPrice, getCustomer } from "../services/Server";
import { TransitionPresets } from "react-navigation-stack";
import BalanceList from "_components/BalanceList";
import { CancelToken } from "axios";

const CryptoPickerScreen = props => {
  const [user, setUser] = useState(props.navigation.getParam("user"));
  const [customer, setCustomer] = useState([]);
  const [cryptoBalances, setCryptoBalances] = useState([]);
  const [cryptoPrices, setPrices] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let mounted = true;
    const userData = props.navigation.getParam("user");
    const customerCode = props.navigation.getParam("customerCode");

    const fetchData = async () => {
      setIsLoading(true);
      const customer = await getCustomer(userData, customerCode);
      const customerBalance = await getBalance(userData, customerCode);
      const filteredCurrencies = customerBalance.cryptoBalances.filter(
        crypto => {
          return (
            crypto.code !== props.navigation.getParam("sourceCurrencyCode")
          );
        }
      );

      if (mounted) {
        setCryptoBalances(filteredCurrencies);
        setCustomer(customer);
        setPrices(await getPrice(userData));
        setIsLoading(false);
      }
    };

    fetchData();
    return () => {
      mounted = false;
    };
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <Content padder>
          <BalanceList
            list={cryptoBalances}
            prices={cryptoPrices}
            customer={customer}
            user={user}
            type="crypto"
            navigation={props.navigation}
            fromCryptoPickerScreen
            setChosenSwapCrypto={props.navigation.getParam(
              "setChosenSwapCrypto"
            )}
            isLoading={isLoading}
          />
        </Content>
      </Container>
    </SafeAreaView>
  );
};

CryptoPickerScreen.navigationOptions = ({ navigation }) => {
  return {
    // ...TransitionPresets.SlideFromLeftIOS,
    // gestureDirection: "horizontal",
    header: (
      <Header darkTheme>
        <Left>
          <Button
            transparent
            onPress={() => {
              navigation.navigate("SwapScreen");
            }}
          >
            <Icon name="arrow-back" />
          </Button>
        </Left>
        <Body>
          <Title>Select crypto</Title>
        </Body>
        <Right />
      </Header>
    )
  };
};

export default CryptoPickerScreen;
