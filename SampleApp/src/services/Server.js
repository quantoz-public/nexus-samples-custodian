import React from "react";
import Config from "Config";
import axios, { CancelToken } from "axios";

const convertToCamelCaseAuthData = json_auth_data => {
  return {
    accessToken: json_auth_data.access_token,
    expiresIn: json_auth_data.expires_in,
    tokenType: json_auth_data.token_type,
    refreshToken: json_auth_data.refresh_token,
    scope: json_auth_data.scope
  };
};

const convertToSearchParams = params => {
  return Object.keys(params)
    .map(key => {
      return encodeURIComponent(key) + "=" + encodeURIComponent(params[key]);
    })
    .join("&");
};

export const authorize = async (config, { user, password }) => {
  var params = [];
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;
  params["grant_type"] = config.grantType;
  params["scope"] = config.scopes.join(" ");
  params["username"] = user;
  params["password"] = password;

  var requestOptions = {
    url: config.serviceConfiguration.tokenEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    data: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return convertToCamelCaseAuthData(response.data);
};

export const refresh = async (config, { refreshToken }) => {
  var params = [];
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;
  params["grant_type"] = config.grantType;
  params["refresh_token"] = refreshToken;
  params["scope"] = config.scopes.join(" ");

  var requestOptions = {
    url: config.serviceConfiguration.tokenEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    data: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return convertToCamelCaseAuthData(response.data);
};

export const revoke = async (config, { tokenToRevoke }) => {
  var params = [];
  params["token"] = tokenToRevoke;
  params["client_id"] = config.clientId;
  params["client_secret"] = config.clientSecret;

  var requestOptions = {
    url: config.serviceConfiguration.revocationEndpoint,
    method: "post",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: convertToSearchParams(params),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// registerRequest:
// {
// 	"email": "jansen@8.nl",
// 	"password": "Pass123$",
// 	"cryptoAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP"
// }
export const registerUser = async registerRequest => {
  let requestOptions = {
    url: "/customers",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    data: JSON.stringify({
      ...registerRequest,
      AnonymousRequestToken: Config.acceptedAnonymousRequestToken
    }),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// createPaymentRequest:
// {
// 	"fromAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
// 	"toAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
//  "tokenCode": <DEFAULT_ASSET>,
//  "amount": "2.93",
//  "memo": "1 bread"
// }
export const createPayment = async (user, createPaymentRequest) => {
  let requestOptions = {
    url: "/transactions/payments",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + user.token.accessToken
    },
    data: JSON.stringify(createPaymentRequest),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

// createPaymentRequest:
// {
// 	"fromAddress": "GCSKHQ2FLBDS2SQLRHWW3PBMQLJVFT7JUI57MLVSN4CSWXLYZVYKLINP",
//  "tokenCode": <DEFAULT_ASSET>,
//  "amount": "2.93",
// }
export const createPayout = async (user, createPayoutRequest) => {
  let requestOptions = {
    url: "/transactions/payouts",
    baseURL: Config.SAMPLE_API,
    method: "post",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + user.token.accessToken
    },
    data: JSON.stringify(createPayoutRequest),
    validateStatus: function(status) {
      return status < 300; // Resolve only if the status code is 200
    }
  };

  let response = await axios.request(requestOptions);
  return response.data;
};

export const getCustomer = async (user, customerCode) => {
  const response = await axios.get(
    Config.SAMPLE_API + "/customers/" + customerCode + "/accounts",
    {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      }
    }
  );
  const json = JSON.parse(JSON.stringify(response.data));
  return json;
};

export const getBalance = async (user, customerCode) => {
  let cancelToken;

  //Check if there are any previous pending requests
  if (typeof cancelToken != typeof undefined) {
    cancelToken.cancel("Operation canceled due to new request.");
  }

  //Save the cancel token for the current request
  cancelToken = axios.CancelToken.source();

  try {
    const response = await axios.get(
      Config.SAMPLE_API + "/balance/customer/accounts/" + customerCode,
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + user.token.accessToken
        }
      },
      {
        cancelToken: cancelToken.token
      }
    );
    return response.data;
  } catch (err) {
    if (axios.isCancel(err)) {
      return console.info(err);
    }
  }
};

export const getPrice = async user => {
  let cancelToken;

  //Check if there are any previous pending requests
  if (typeof cancelToken != typeof undefined) {
    cancelToken.cancel("Operation canceled due to new request.");
  }

  //Save the cancel token for the current request
  cancelToken = axios.CancelToken.source();

  console.log("[Server.js - getPrice] user: ", user);

  try {
    const response = await axios.get(
      Config.SAMPLE_API + "/price/eur/",
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + user.token.accessToken
        }
      },
      {
        cancelToken: cancelToken.token
      }
    );
    const prices = [];
    if (response.status === 200) {
      let data = response.data.prices;
      const keys = Object.keys(data);
      keys.forEach(key => {
        prices.push({
          code: key,
          buy: (data[key].buy + data[key].sell) / 2
        });
      });
      return prices;
    } else {
      // crypto price is not defined
      return null;
    }
  } catch (err) {
    if (axios.isCancel(err)) {
      return console.info(err);
    }
  }
};

export const getTransactions = async (
  user,
  cryptoCode,
  accountCode,
  currencyCode,
  customerCode
) => {
  const response = await axios.get(
    Config.SAMPLE_API +
      "/transactions/custodian?cryptoCode=" +
      cryptoCode +
      "&accountCode=" +
      accountCode +
      "&currencyCode=" +
      currencyCode +
      "&customerCode=" +
      customerCode,
    {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      }
    }
  );
  return response.data;
};

export const createBuyTransaction = async (user, createBuyRequest) => {
  try {
    let requestOptions = {
      url: "/transactions/buy/",
      baseURL: Config.SAMPLE_API,
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      },
      data: JSON.stringify(createBuyRequest),
      validateStatus: function(status) {
        return status < 300; // Resolve only if the status code is 200
      }
    };

    let response = await axios.request(requestOptions);
    return response.data;
  } catch (error) {
    console.log(error.response);
    return null;
  }
};

export const createSellTransaction = async (user, createSellRequest) => {
  try {
    let requestOptions = {
      url: "/transactions/sell/",
      baseURL: Config.SAMPLE_API,
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      },
      data: JSON.stringify(createSellRequest),
      validateStatus: function(status) {
        return status < 300; // Resolve only if the status code is 200
      }
    };

    let response = await axios.request(requestOptions);
    return response.data;
  } catch (error) {
    console.log(error.response);
    return null;
  }
};

export const createSwapTransaction = async (user, createSwapRequest) => {
  try {
    let requestOptions = {
      url: "/transactions/swap/",
      baseURL: Config.SAMPLE_API,
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      },
      data: JSON.stringify(createSwapRequest),
      validateStatus: function(status) {
        return status < 300; // Resolve only if the status code is 200
      }
    };

    let response = await axios.request(requestOptions);
    return response.data;
  } catch (error) {
    console.log(error.response);
    return null;
  }
};

// action name : buy, sell or swap
export const getPaymentMethodCode = (cryptoCode, actionName) => {
  let pmCode =
    "CD_" +
    actionName.toUpperCase() +
    "_GENERIC_" +
    cryptoCode.toUpperCase() +
    "_EUR";
  return pmCode;
};

export const getCryptoPrice = async (user, cryptoCode) => {
  try {
    const response = await axios.get(
      Config.SAMPLE_API + "/price/eur/" + cryptoCode,
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: "Bearer " + user.token.accessToken
        }
      }
    );
    let price = {};
    if (response.status === 200) {
      let data = response.data.prices;
      price = {
        buy: data[cryptoCode].buy,
        sell: data[cryptoCode].sell
      };
    }
    return price;
  } catch (error) {
    console.log(error);
  }
};

export const getBuyTransactionFee = async (user, createBuyRequest) => {
  try {
    let requestOptions = {
      url: "/transactions/simulate/buy",
      baseURL: Config.SAMPLE_API,
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      },
      data: JSON.stringify(createBuyRequest),
      validateStatus: function(status) {
        return status < 300; // Resolve only if the status code is 200
      }
    };

    let response = await axios.request(requestOptions);
    return response.data;
  } catch (error) {
    console.log(error.response);
    return null;
  }
};

export const getSellTransactionFee = async (user, createSellRequest) => {
  try {
    let requestOptions = {
      url: "/transactions/simulate/sell",
      baseURL: Config.SAMPLE_API,
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + user.token.accessToken
      },
      data: JSON.stringify(createSellRequest),
      validateStatus: function(status) {
        return status < 300; // Resolve only if the status code is 200
      }
    };

    let response = await axios.request(requestOptions);
    return response.data;
  } catch (error) {
    console.log(error.response);
    return null;
  }
};
