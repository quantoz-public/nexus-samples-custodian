import React from "react";
import { StyleSheet } from "react-native";
import { List, ListItem, Body, Text, Spinner } from "native-base";
import TransactionListItem from "_components/TransactionListItem";

const styles = StyleSheet.create({
  noTransactionsMessage: {
    marginTop: 32,
    textAlign: "center"
  }
});

const TransactionsList = props => {
  renderList = () =>
    props.transactions === undefined || props.transactions.length === 0 ? (
      props.isLoading ? (
        <Spinner size="large" />
      ) : (
        <ListItem>
          <Body>
            <Text label-light style={styles.noTransactionsMessage}>
              No transactions to show yet.
            </Text>
          </Body>
        </ListItem>
      )
    ) : (
      props.transactions.map((transaction, index) => {
        if (props.currencyType !== "crypto" && transaction.type === "SWAP") {
          return null;
        } else {
          return (
            <TransactionListItem
              key={index}
              code={transaction.cryptoCode}
              currencyType={props.currencyType}
              direction={transaction.direction}
              address={transaction.destinationCryptoAddress}
              date={transaction.created}
              amount={
                props.currencyType === "crypto"
                  ? transaction.executedAmounts.cryptoAmount
                  : transaction.executedAmounts.fiatValue
              }
              fiatAmount={
                props.currencyType === "crypto"
                  ? transaction.executedAmounts.fiatValue
                  : null
              }
              transactionType={transaction.type}
              date={transaction.created}
            />
          );
        }
      })
    );

  return (
    <List>
      {renderList()}
      {props.children}
    </List>
  );
};

export default TransactionsList;
