﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;

namespace CustodianSampleApi.Logic.Interfaces
{
    public interface IPriceLogic
    {
        Task<GetPrices> GetPrice(string currencyCode);
        Task<GetPrices> GetPrice(string currencyCode, string cryptoCode);
    }
}
