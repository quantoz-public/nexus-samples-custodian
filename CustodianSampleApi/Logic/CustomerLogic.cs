﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using IdentityModel;
using CustodianSampleApi.Data.DTOs.Logic.Customers;
using CustodianSampleApi.Data.DTOs.Repo.Customers;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Models;
using CustodianSampleApi.Services;
using CustodianSampleApi.Helpers;
using CustodianSampleApi.Data.DTOs.Logic.Customer;

namespace CustodianSampleApi.Logic
{
    public class CustomerLogic : ICustomerLogic
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly INexusApiService _nexusApiService;

        public CustomerLogic(
            UserManager<ApplicationUser> userManager,
            INexusApiService nexusApiService)
        {
            _userManager = userManager;
            _nexusApiService = nexusApiService;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<ConfirmCustomerDto> Create(CreateCustomerDto dto)
        {
            var email = dto.Email.Trim();
            var password = dto.Password.Trim();

            // check if there is a login for this customer
            // NO - create log-in and continue with 1
            // YES:
            //  1. Is there an Active Nexus Customer for this email?
            //   YES - throw exception: user exists
            //   NO - create customer in Nexus and continue

            // We id users by email, IdentityServer4 by name, hence the `UserName = Email`
            if (!await UserLoginFoundByNameAsync(email))
            {
                await CreateNewUserLoginAsync(email, password);
            }

            // we use the email as customerCode
            var customerCode = Tools.TransformEmailToText(email);

            if (await NexusCustomerFoundByNameAsync(customerCode))
            {
                throw new ArgumentException("User with this Email exists.");
            }
            else
            {
                return await CreateNexusCustomerAsync(customerCode, email);
            }
        }

        private async Task<ConfirmCustomerDto> CreateNexusCustomerAsync(string customerCode, string email)
        {
            var customer = await _nexusApiService.CreateCustomer(customerCode,
                email);

            // only 1 Account is created, so we can take First()
            var account = customer.Accounts.First();

            return new ConfirmCustomerDto
            {
                Email = customer.Email,
                CryptoAddress = account.CustomerCryptoAddress
            };
        }

        private async Task<bool> NexusCustomerFoundByNameAsync(string customerCode)
        {
            return (await _nexusApiService.GetCustomer(customerCode) != null);
        }

        private async Task<bool> UserLoginFoundByNameAsync(string userName)
        {
            return (await _userManager.FindByNameAsync(userName) != null);
        }

        private async Task CreateNewUserLoginAsync(string email, string password)
        {
            var user = new ApplicationUser
            {
                UserName = email
            };

            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                if (result.Errors.First().Code == "PasswordRequiresNonAlphanumeric" ||
                   result.Errors.First().Code == "PasswordRequiresLower" ||
                   result.Errors.First().Code == "PasswordRequiresUpper" ||
                   result.Errors.First().Code == "PasswordRequiresDigit")
                {
                    throw new ArgumentException(result.Errors.First().Description);
                }
                throw new Exception(result.Errors.First().Description);
            }

            result = await _userManager.AddClaimsAsync(user, new Claim[]
            {
                    new Claim(JwtClaimTypes.Name, email)
            });
            if (!result.Succeeded)
            {
                throw new Exception(result.Errors.First().Description);
            }
        }

        public Task<ConfirmCustomerDto> Get(int deviceId)
        {
            throw new NotImplementedException();
        }

        public async Task<CustomerAccountsDTO> GetCustomerAccounts(string customerCode)
        {
            return await _nexusApiService.GetCustomerAccounts(customerCode);
        }
    }
}
