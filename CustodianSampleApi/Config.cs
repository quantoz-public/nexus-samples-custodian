﻿using System.Collections.Generic;
using IdentityServer4;
using IdentityServer4.Models;

namespace CustodianSampleApi
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                new ApiResource(IdentityServerConstants.LocalApi.ScopeName)
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "client",
                    ClientName = "Password Client",
                    ClientSecrets = { new Secret("82248ea2-f5a9-46a0-972a-91b15ec90ea2".Sha256()) },
                    RedirectUris = { "com.quantoznexus.testidentity://oauthredirect" },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = { IdentityServerConstants.LocalApi.ScopeName },
                    AllowOfflineAccess = true
                }
            };
    }
}