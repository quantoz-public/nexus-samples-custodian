import React from "react";
import { usePromiseTracker } from "react-promise-tracker";
import { Spinner } from "native-base";

export const LoadingSpinner = props => {
  const { promiseInProgress } = usePromiseTracker({ area: props.area });
  if (promiseInProgress === true) {
    return <Spinner />;
  } else {
    return props.children;
  }
};

export { trackPromise } from "react-promise-tracker";
