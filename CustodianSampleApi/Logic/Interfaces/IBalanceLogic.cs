﻿using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;

namespace CustodianSampleApi.Logic.Interfaces
{
    public interface IBalanceLogic
    {
        Task<BalanceDTO> GetBalance(string customerCode);
    }
}
