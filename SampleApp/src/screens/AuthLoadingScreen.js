import React, { useEffect } from "react";
import { ActivityIndicator, StatusBar, View } from "react-native";

export default function AuthLoadingScreen({ navigation }) {
  useEffect(() => {
    navigation.navigate("Auth");
  }, []);

  return (
    <View>
      <StatusBar barStyle="default" />
      <ActivityIndicator />
    </View>
  );
}
