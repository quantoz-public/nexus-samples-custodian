export default {
  SAMPLE_API: "http://10.0.2.2:5000/api",
  networkPassphrase: "Test SDF Network ; September 2015",
  STELLAR_API: "https://horizon-testnet.stellar.org",
  DEFAULT_ASSET: "CEUR",
  decimalPrecision: 2,
  acceptedAnonymousRequestToken: "b5543f93-7a61-40b0-8cc7-bbe900c88243"
};

const issuer = "http://10.0.2.2:5000";
export const authConfig = {
  serviceConfiguration: {
    authorizationEndpoint: `${issuer}/connect/authorize`,
    tokenEndpoint: `${issuer}/connect/token`,
    revocationEndpoint: `${issuer}/connect/revocation`
  },
  clientId: "client",
  clientSecret: "82248ea2-f5a9-46a0-972a-91b15ec90ea2",
  scopes: ["IdentityServerApi", "offline_access"],
  dangerouslyAllowInsecureHttpRequests: true,
  grantType: "password",
  redirectUrl: "com.quantoznexus.testidentity://oauthredirect"
};
