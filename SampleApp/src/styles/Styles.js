import { StyleSheet, Platform } from "react-native";
import { Colors } from "_styles";

export default StyleSheet.create({
  container: {
    marginTop: 0,
    marginBottom: 16,
    marginLeft: 16,
    marginRight: 16
  },
  container__fill: {
    flex: 1
  },
  container__background: {
    backgroundColor: Colors.white
  },
  backgroundImage: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },
  color__dim_white: {
    color: Colors.lightGray
  },

  // UI ELEMENTS
  button__primary__disabled: {
    backgroundColor: "rgba(250,185,44, 0.4)",
    color: "gray"
  },
  button__text: {
    borderWidth: 0,
    borderColor: "transparent",
    color: "#FAB92C"
  },
  button__small: {
    alignSelf: "center",
    height: 30
  },
  button__text__small: {
    fontSize: 10
  },
  card: {
    flexGrow: 1,
    backgroundColor: Colors.darkGray,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.25,
    shadowRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 24,
    paddingBottom: 24
  },
  card__image: {
    maxHeight: Platform.OS == "android" ? 135 : 200
  },
  form__input: {
    height: 64,
    paddingLeft: 16,
    paddingTop: 20,
    paddingBottom: 40
  },

  // TYPOGRAPHY

  screenTitle: {
    fontWeight: "500",
    fontSize: 34,
    lineHeight: 40,
    letterSpacing: -0.02
  },
  cardTitle: {
    fontWeight: "300",
    fontSize: 28,
    lineHeight: 32,
    letterSpacing: -0.02,
    textAlign: "center",
    color: "#DDDAE5",
    marginTop: 24
  },
  buttonText: {
    fontWeight: "500",
    fontSize: 17,
    lineHeight: 24,
    textAlign: "center"
  },
  labelText: {
    fontWeight: "300",
    fontSize: 13,
    lineHeight: 20,
    letterSpacing: -0.02,
    textTransform: "uppercase",
    color: Colors.lightGray
  },
  listText: {
    fontWeight: "500",
    fontSize: 13,
    lineHeight: 16,
    letterSpacing: -0.02,
    color: "#DDDAE5"
  },
  listText__secondary: {
    fontWeight: "300",
    fontSize: 11,
    lineHeight: 16,
    letterSpacing: -0.06,
    color: "#7B7B7B"
  },
  listTextBig: {
    fontWeight: "500",
    fontSize: Platform.OS == "android" ? 21 : 17,
    lineHeight: 20,
    letterSpacing: -0.02,
    color: "#DDDAE5"
  }
});
