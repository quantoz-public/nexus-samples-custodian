﻿namespace CustodianSampleApi.Data.DTOs.Logic.Transaction
{
    public class SellTransactionDTO
    {
        public string CustomerCode { get; set; }

        public string AccountCode { get; set; }

        public string CurrencyCode { get; set; }

        public string CryptoCode { get; set; }

        public decimal FiatValue { get; set; }

        public decimal? RequestedPrice { get; set; }

        public string PaymentMethodCode { get; set; }
    }
}
