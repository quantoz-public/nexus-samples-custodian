﻿namespace CustodianSampleApi.Data.DTOs.Logic.Transaction
{
    public class GetTransactionDTO
    {
        public string DestinationCryptoAddress { get; set; }
        public string CryptoCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Direction { get; set; }
        public string Status { get; set; }
        public ExecutedAmountDTO ExecutedAmounts { get; set;}
        public string Type { get; set; }
        public string Created { get; set; }
    }

    public class ExecutedAmountDTO 
    { 
        public double? CryptoAmount { get; set; }
        public double? FiatValue { get; set; }
    }
}
