﻿using System.ComponentModel.DataAnnotations;

namespace CustodianSampleApi.Data.DTOs.Controller.Transaction
{
    public class SellTransaction
    {
        /// <summary>
        /// Unique code of the customer for which the buy transaction is created.
        /// </summary>
        [Required]
        public string CustomerCode { get; set; }

        /// <summary>
        /// Nexus generated unique identifier of the account or combination of crypto code and address.
        /// Example: XLM-14DuxbRVYPSmt1c3f2UnoKVUHkR5bVB9oa
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Three letter code of the crypto currency which the transaction is processed in.
        /// </summary>
        [Required]
        [StringLength(3)]
        public string CryptoCode { get; set; }

        /// <summary>
        /// The amount of crypto currency the sell transaction should be created for.
        /// </summary>
        [Required]
        public decimal CryptoAmount { get; set; }

        /// <summary>
        /// Three letter code of the currency which the transaction is paid out in.
        /// </summary>
        [Required]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The amount of fiat currency the buy transaction should be created for.
        /// </summary>
        [Required]
        public decimal FiatValue { get; set; }

        /// <summary>
        /// The SELL price at which the crypto currency will be sold.
        /// Nexus checks if the requested SELL price is within certain ranges (Wiggle room).
        /// Ranges:
        /// 5% price increase allowed
        /// 10% price reduction allowed
        /// </summary>
        public decimal? RequestedPrice { get; set; }

        /// <summary>
        /// Code of the payment method which the transaction is processed with.
        /// For a custodian buy transaction, the payment method is strictly used for calculating fees only
        /// and will not process requests with the payment provider.
        /// </summary>
        [Required]
        public string PaymentMethodCode { get; set; }
    }
}
