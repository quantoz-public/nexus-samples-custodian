import React from "react";
import { Platform } from "react-native";
import { createStackNavigator } from "react-navigation";

import GenericOverviewScreen from "_screens/GenericOverviewScreen";
import CurrencyOverviewScreen from "_screens/CurrencyOverviewScreen";
import TransactionsScreen from "_screens/TransactionsScreen";
import ConfirmationScreen from "_screens/ConfirmationScreen";
import PaymentScreen from "_screens/PaymentScreen";
import PayoutScreen from "_screens/PayoutScreen";
import ScanScreen from "_screens/ScanScreen";
import ReceiveScreen from "_screens/ReceiveScreen";
import BuyScreen from "_screens/BuyScreen";
import SellScreen from "_screens/SellScreen";
import SwapScreen from "_screens/SwapScreen";
import CryptoPickerScreen from "_screens/CryptoPickerScreen";

const config = Platform.select({
  web: { headerMode: "screen" },
  default: {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: "#302D2B"
      },
      headerTintColor: "#DDDAE5",
      headerTitleStyle: {
        fontWeight: "500",
        fontSize: 34,
        lineHeight: 40,
        letterSpacing: -0.02
      }
    }
  }
});

const PaymentStack = createStackNavigator({
  Pay: PaymentScreen,
  PaymentConfirmed: {
    screen: ConfirmationScreen,
    navigationOptions: {
      header: null
    }
  }
});

const PayoutStack = createStackNavigator({
  Payout: PayoutScreen,
  PayoutConfirmed: {
    screen: ConfirmationScreen,
    navigationOptions: {
      header: null
    }
  }
});

const mainStack = createStackNavigator({
  GenericOverviewScreen: GenericOverviewScreen,
  CurrencyOverviewScreen: CurrencyOverviewScreen,
  ConfirmationScreen: {
    screen: ConfirmationScreen,
    navigationOptions: {
      header: null
    }
  },
  BuyScreen: BuyScreen,
  SellScreen: SellScreen,
  SwapScreen: SwapScreen,
  CryptoPickerScreen: CryptoPickerScreen,
  Transactions: TransactionsScreen,
  PaymentStack: {
    screen: PaymentStack,
    navigationOptions: {
      header: null,
      headerMode: "none"
    }
  },
  ScanScreen: {
    screen: ScanScreen,
    navigationOptions: {
      header: null
    }
  },
  PayoutStack: {
    screen: PayoutStack,
    navigationOptions: {
      header: null
    }
  },
  ReceiveScreen: ReceiveScreen
});

mainStack.path = "";

export default mainStack;
