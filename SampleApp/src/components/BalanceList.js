import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { List, Text, Spinner } from "native-base";
import BalanceListItem from "_components/BalanceListItem";

const styles = StyleSheet.create({
  balanceListLabel: {
    paddingBottom: 16
  },
  noLeftMargin: {
    marginLeft: 0
  },
  icon: {
    color: "#02182B"
  }
});

const calculateCryptoValue = (price, code, total) => {
  if (price !== undefined && price.length > 0) {
    for (var i = 0; i < price.length; i++) {
      if (price[i].code === code) {
        return Number(price[i].buy * total);
      }
    }
  }
  return 0;
};

const calculateEurAmount = (price, code) => {
  if (price !== undefined && price.length > 0) {
    for (var i = 0; i < price.length; i++) {
      if (price[i].code === code) {
        return Number(price[i].buy);
      }
    }
  }
  return 0;
};

const BalanceList = props => {
  let account = {};
  displayBalanceListItem = () => {
    return props.list.map((item, index) => {
      if (
        props.customer !== undefined &&
        props.customer.length > 0 &&
        props.type === "crypto"
      ) {
        account = props.customer.find(acc => acc.dcCode === item.code);
      }

      return (
        <BalanceListItem
          key={item.code}
          currencyType={props.type}
          customer={props.customer != null ? props.customer : null}
          user={props.user}
          code={item.code}
          navigation={props.navigation}
          name={item.name}
          balance={props.type !== "crypto" ? props.fiatBalance : item.total}
          cryptoValue={
            props.type === "crypto"
              ? calculateCryptoValue(props.prices, item.code, item.total)
              : null
          }
          euroAmount={
            props.type === "crypto"
              ? calculateEurAmount(props.prices, item.code)
              : null
          }
          fromCryptoPickerScreen={
            props.fromCryptoPickerScreen !== undefined
              ? props.fromCryptoPickerScreen
              : null
          }
          setChosenSwapCrypto={props.setChosenSwapCrypto}
          accountCode={
            typeof account === "undefined" ? "" : account.accountCode
          }
          customerCode={props.customerCode}
          fiatBalance={props.fiatBalance}
          cryptoBalance={item.total}
        />
      );
    });
  };

  return (
    <List>
      <Text label-bold-light style={styles.balanceListLabel}>
        {props.type === "fiat" ? "Fiat currencies" : "Crypto currencies"}
      </Text>
      {props.isLoading ? <Spinner size="large" /> : displayBalanceListItem()}
    </List>
  );
};

export default BalanceList;
