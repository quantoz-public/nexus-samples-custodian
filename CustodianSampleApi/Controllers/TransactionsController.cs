﻿using System;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using CustodianSampleApi.Data;
using CustodianSampleApi.Data.DTOs.Controller.Transaction;
using CustodianSampleApi.Data.DTOs.Logic.Transaction;
using CustodianSampleApi.Data.Pocos;
using CustodianSampleApi.Helpers;
using CustodianSampleApi.Logic.Interfaces;
using CustodianSampleApi.Models;
using static IdentityServer4.IdentityServerConstants;

namespace CustodianSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ITransactionLogic _transactionLogic;

        public TransactionsController(
            UserManager<ApplicationUser> userManager,
            ITransactionLogic transactionLogic)
            : base()
        {
            _userManager = userManager;
            _transactionLogic = transactionLogic;
        }

        [HttpGet("custodian")]
        public async Task<ActionResult<GetTransactionDTO>> GetTransactions(
            [FromQuery] string cryptoCode,
            [FromQuery] string accountCode,
            [FromQuery] string customerCode,
            [FromQuery] string currencyCode,
            [FromQuery] string status = "COMPLETED")
        {
            var result = await _transactionLogic.GetTransactions(cryptoCode, accountCode, status, currencyCode, customerCode);

            if (result == null)
            {
                return BadRequest($"Transactions of {cryptoCode} in {currencyCode} is not found");
            }

            return Ok(result);
        }

        [HttpPost("buy")]
        public async Task<IActionResult> Buy(BuyTransaction request)
        {
            var result = await _transactionLogic.CreateBuyTransaction(new BuyTransactionDTO
            {
                CurrencyCode =  request.CurrencyCode,
                AccountCode = request.AccountCode,
                CustomerCode = request.CustomerCode,
                FiatValue = request.FiatValue,
                PaymentMethodCode = request.PaymentMethodCode,
                RequestedPrice = request.RequestedPrice,
                CryptoCode = request.CryptoCode
            });

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest($"Requested buy transactions could not be created.");
            }
        }

        [HttpPost("sell")]
        public async Task<IActionResult> Sell(SellTransaction request)
        {
            var result = await _transactionLogic.CreateSellTransaction(new SellTransactionDTO
            {
                CurrencyCode = request.CurrencyCode,
                AccountCode = request.AccountCode,
                CustomerCode = request.CustomerCode,
                FiatValue = request.FiatValue,
                PaymentMethodCode = request.PaymentMethodCode,
                RequestedPrice = request.RequestedPrice,
                CryptoCode = request.CryptoCode
            });

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest($"Requested sell transaction could not be created.");
            }
        }

        [HttpPost("swap")]
        public async Task<IActionResult> Swap(SwapTransaction request)
        {
            var result = await _transactionLogic.CreateSwapTransaction(new SwapTransactionDTO
            {
                PaymentMethodCode = request.PaymentMethodCode,
                CustomerCode = request.CustomerCode,
                Source = new SwapSourceDTO
                { 
                    AccountCode = request.Source.AccountCode,
                    CryptoCode = request.Source.CryptoCode,
                    CryptoAmount = request.Source.CryptoAmount,
                    RequestedPrice = request.Source.RequestedPrice
                },
                Destination = new SwapDestinationDTO
                {
                    AccountCode = request.Destination.AccountCode,
                    CryptoCode = request.Destination.CryptoCode,
                    RequestedPrice = request.Destination.RequestedPrice
                }
            });

            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest($"Requested swap transaction could not be created.");
            }
        }

        [HttpPost("simulate/buy")]
        public async Task<IActionResult> SimulateBuy(BuyTransaction request)
        {
            var result = await _transactionLogic.SimulateBuyTransaction(new BuyTransactionDTO
            {
                CurrencyCode = request.CurrencyCode,
                AccountCode = request.AccountCode,
                CustomerCode = request.CustomerCode,
                FiatValue = request.FiatValue,
                PaymentMethodCode = request.PaymentMethodCode,
                RequestedPrice = request.RequestedPrice,
                CryptoCode = request.CryptoCode
            });

            if (result != null)
            {
                var Fees = result.Transaction.Fees;

                return Ok(new
                {
                    Fee = Fees.NetworkFeeCrypto.GetValueOrDefault()
                    + Fees.NetworkFeeFiat.GetValueOrDefault()
                    + Fees.PartnerFeeAmount.GetValueOrDefault()
                    + Fees.BankFeeAmount.GetValueOrDefault(),
                    result.Transaction.ExecutedAmounts.CryptoAmount
                });
            }
            else
            {
                return BadRequest($"Fee of this transaction could not be found.");
            }
        }

        [HttpPost("simulate/sell")]
        public async Task<IActionResult> SimulateSell(SellTransaction request)
        {
            var result = await _transactionLogic.SimulateSellTransaction(new SellTransactionDTO
            {
                CurrencyCode = request.CurrencyCode,
                AccountCode = request.AccountCode,
                CustomerCode = request.CustomerCode,
                FiatValue = request.FiatValue,
                PaymentMethodCode = request.PaymentMethodCode,
                RequestedPrice = request.RequestedPrice,
                CryptoCode = request.CryptoCode
            });

            if (result != null)
            {
                var Fees = result.Transaction.Fees;

                return Ok(new
                {
                    Fee = Fees.NetworkFeeCrypto.GetValueOrDefault()
                    + Fees.NetworkFeeFiat.GetValueOrDefault()
                    + Fees.PartnerFeeAmount.GetValueOrDefault()
                    + Fees.BankFeeAmount.GetValueOrDefault(),
                    result.Transaction.ExecutedAmounts.CryptoAmount
                });
            }
            else
            {
                return BadRequest($"Fee of this transaction could not be found.");
            }
        }
    }
}
