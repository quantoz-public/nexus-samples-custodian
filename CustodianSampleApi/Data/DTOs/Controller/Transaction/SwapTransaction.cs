﻿using System.ComponentModel.DataAnnotations;

namespace CustodianSampleApi.Data.DTOs.Controller.Transaction
{
    public class SwapTransaction
    {
        /// <summary>
        /// PaymentMethod to handle payment validation.
        /// </summary>
        /// <example>
        /// EX_GENERIC_BTC_EUR
        /// </example>
        [Required]
        public string PaymentMethodCode { get; set; }

        /// <summary>
        /// Unique code of the customer for which the swap transaction is created.
        /// </summary>
        [Required]
        public string CustomerCode { get; set; }

        [Required]
        public SwapSource Source { get; set; }

        [Required]
        public SwapDestination Destination { get; set; }
    }

    public class SwapSource
    {
        /// <summary>
        /// Nexus generated unique identifier of the account or combination of crypto code and address.
        /// Example: XLM-14DuxbRVYPSmt1c3f2UnoKVUHkR5bVB9oa
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Three letter code of the crypto currency which the transaction is processed in.
        /// </summary>
        [Required]
        public string CryptoCode { get; set; }

        /// <summary>
        /// The amount of crypto currency.
        /// </summary>
        [Required]
        public decimal CryptoAmount { get; set; }

        /// <summary>
        /// The SELL price at which the crypto currency will be swapped from.
        /// Nexus checks if the requested SELL price is within certain ranges (Wiggle room).
        /// Ranges:
        /// 5% price increase allowed
        /// 10% price reduction allowed
        /// </summary>
        public decimal? RequestedPrice { get; set; }
    }

    public class SwapDestination
    {
        /// <summary>
        /// Nexus generated unique identifier of the account or combination of crypto code and address.
        /// Example: XLM-14DuxbRVYPSmt1c3f2UnoKVUHkR5bVB9oa
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Three letter code of the crypto currency which the transaction is processed in.
        /// </summary>
        [Required]
        [StringLength(3)]
        public string CryptoCode { get; set; }

        /// <summary>
        /// The BUY price at which the crypto currency will be swapped to.
        /// Nexus checks if the requested BUY price is within certain ranges (Wiggle room).
        /// Ranges:
        /// 10% price increase allowed
        /// 5% price reduction allowed
        /// </summary>
        public decimal? RequestedPrice { get; set; }
    }
}
