﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustodianSampleApi.Data.DTOs.Logic
{
    public class PriceDTO
    {
        public double? Buy { get; set; }
        public double? Sell { get; set; }
        public double? EstimatedNetworkSlowFee { get; set; }
        public double? EstimatedNetworkFastFee { get; set; }
        public DateTime? Updated { get; set; }
    }

    public class GetPrices
    {
        public DateTime Created { get; set; }
        public string CurrencyCode { get; set; }
        public IDictionary<string, PriceDTO> Prices { get; set; }
    }
}
