﻿namespace CustodianSampleApi.Data.DTOs.Logic.Transaction
{
    public class TotalTransactionReponseDTO
    {
        /// <summary>
        /// Resulting new transaction
        /// </summary>
        public TransactionResponseDTO Transaction { get; set; }
    }

    public class TransactionResponseDTO
    {
        public string CustomerCode { get; set; }

        public string AccountCode { get; set; }

        public string CurrencyCode { get; set; }

        public string CryptoCode { get; set; }

        public decimal FiatValue { get; set; }

        public decimal? RequestedPrice { get; set; }

        public string PaymentMethodCode { get; set; }
        public TransactionFeeDTO Fees { get; set; }
        public AmountsResponseDTO ExecutedAmounts { get; set; }
    }

    public class TransactionFeeDTO
    {
        public decimal? PartnerFeeAmount { get; set; }
        public decimal? BankFeeAmount { get; set; }
        public decimal? NetworkFeeFiat { get; set; }
        public decimal? NetworkFeeCrypto { get; set; }
    }

    public class AmountsResponseDTO
    {
        /// <example>
        /// 0.02344567
        /// </example>
        public decimal CryptoAmount { get; set; }

        /// <example>
        /// 50.00
        /// </example>
        public decimal FiatValue { get; set; }

        /// <example>
        /// 4500.00000
        /// </example>
        public decimal? CryptoPrice { get; set; }
    }

    public class CustodianSwapResponseDTO
    {
        /// <summary>
        /// New transaction for sender
        /// </summary>
        public TransactionResponseDTO SourceTransaction { get; set; }

        /// <summary>
        /// New transaction for receiver
        /// </summary>
        public TransactionResponseDTO DestinationTransaction { get; set; }
    }

}
