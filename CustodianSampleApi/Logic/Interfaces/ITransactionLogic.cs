﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic.Transaction;

namespace CustodianSampleApi.Logic.Interfaces
{
    public interface ITransactionLogic
    {
        Task<List<GetTransactionDTO>> GetTransactions(string cryptoCode, string accountCode, string status, string currencyCode, string customerCode);
        Task<TotalTransactionReponseDTO> CreateBuyTransaction(BuyTransactionDTO buyTransaction);
        Task<TotalTransactionReponseDTO> CreateSellTransaction(SellTransactionDTO sellTransaction);
        Task<CustodianSwapResponseDTO> CreateSwapTransaction(SwapTransactionDTO swapTransaction);
        Task<TotalTransactionReponseDTO> SimulateBuyTransaction(BuyTransactionDTO buyTransaction);
        Task<TotalTransactionReponseDTO> SimulateSellTransaction(SellTransactionDTO buyTransaction);
    }
}
