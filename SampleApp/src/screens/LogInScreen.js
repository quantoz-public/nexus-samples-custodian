import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, ImageBackground, Image } from "react-native";
import {
  Container,
  Form,
  Button,
  Label,
  Text,
  Input,
  Item,
  Icon
} from "native-base";
import { Row, Grid } from "react-native-easy-grid";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { logInAsync } from "_services/Authorization";
import { LoadingSpinner, trackPromise } from "_components/LoadingSpinner";
import {
  isUserLoggedInAsync,
  getLoggedInUserEmailAsync,
  getUserAsync,
  noStoredUsers
} from "_services/Storage";
import { ErrorModal } from "_components/Alert";

const styles = StyleSheet.create({
  backgroundImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  noLeftMargin: {
    marginLeft: 0
  }
});

export default function LogInScreen({ navigation }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    // skip the log-in screen, if there are no users in the local storage
    let mounted = true;

    const checkIfFirstUserLogIn = async () => {
      if ((await noStoredUsers()) && mounted) {
        navigation.navigate("SignUpScreen", { email, password });
      }
    };
    checkIfFirstUserLogIn();

    return () => {
      mounted = false;
    };
  }, []);

  _logInAsync = async () => {
    var login = async () => {
      try {
        await logInAsync(email.toLowerCase(), password);

        if (await isUserLoggedInAsync()) {
          var userEmail = await getLoggedInUserEmailAsync();
          var user = await getUserAsync(userEmail);
          if (user === null) {
            throw Error("User cannot be found.");
          }
          navigation.navigate("GenericOverviewScreen", { user, userEmail });
        } else {
          ErrorModal("Log-in Failed, try again");
        }
      } catch (error) {
        ErrorModal(error);
      }
    };

    await trackPromise(login(), "log-in-button");
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Container>
        <ImageBackground
          source={require("../assets/images/background_image_onboarding.png")}
          style={styles.backgroundImage}
        >
          <KeyboardAwareScrollView>
            <Grid>
              <Row
                style={{
                  flexDirection: "column",
                  justifyContent: "flex-end",
                  alignItems: "center",
                  paddingVertical: 40
                }}
              >
                <Image
                  source={require("../assets/images/quantoz_logo_white.png")}
                />
                <Text notification-dark style={{ marginTop: 24 }}>
                  Custodian sample app
                </Text>
              </Row>
              <Row style={{ flexDirection: "column", padding: 16 }}>
                <Form>
                  <Item floatingLabel style={styles.noLeftMargin}>
                    <Label>E-mail</Label>
                    <Input
                      value={email}
                      onChangeText={setEmail}
                      returnKeyType={"next"}
                      getRef={c => (this.email = c)}
                      onSubmitEditing={() => this.password._root.focus()}
                      blurOnSubmit={false}
                      keyboardType="email-address"
                    />
                  </Item>
                  <Item
                    floatingLabel
                    style={[styles.noLeftMargin, { marginBottom: 24 }]}
                  >
                    <Label>Password</Label>
                    <Input
                      secureTextEntry={true}
                      value={password}
                      onChangeText={setPassword}
                      returnKeyType={"next"}
                      getRef={c => (this.password = c)}
                      onSubmitEditing={() => _logInAsync()}
                      blurOnSubmit={false}
                    />
                  </Item>
                </Form>
                <LoadingSpinner area="log-in-button">
                  <Button
                    block
                    primary-dark
                    style={{ marginBottom: 16 }}
                    onPress={() => {
                      _logInAsync();
                    }}
                  >
                    <Text>Log In</Text>
                  </Button>
                </LoadingSpinner>
                <Button block secondary-dark>
                  <Text
                    primary
                    onPress={() => {
                      navigation.navigate("SignUpScreen", { email, password });
                    }}
                  >
                    Sign up
                  </Text>
                </Button>
              </Row>
            </Grid>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </Container>
    </SafeAreaView>
  );
}

LogInScreen.navigationOptions = {
  title: "Login",
  headerBackTitle: null
};
