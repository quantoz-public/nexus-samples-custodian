import { Alert } from "react-native";

export const ErrorModal = (textOrErrorObject, okFun) => {
  var title = "-";
  var error = "-";
  var errorType = typeof textOrErrorObject;

  if (errorType === "string") {
    title = "Error";
    error = textOrErrorObject;
  } else if (typeof textOrErrorObject.response === "object") {
    if (typeof textOrErrorObject.response.data === "object") {
      var responseBody = textOrErrorObject.response.data;
      if (typeof responseBody.error === "string") {
        // authorization error
        title = responseBody.error;
        error = responseBody.error_description;
      } else if (typeof responseBody.errors === "object") {
        // TokenSampleAPI error
        title = responseBody.title;
        error = JSON.stringify(responseBody.errors, null, 2);
      } else if (typeof responseBody.detail === "string") {
        title = responseBody.title;
        error = responseBody.detail;
      } else {
        error = JSON.stringify(responseBody, null, 2);
      }
    } else if (typeof textOrErrorObject.response.data === "string") {
      title = textOrErrorObject.message;
      error = textOrErrorObject.response.data;
    } else {
      error = JSON.stringify(textOrErrorObject, null, 2);
    }
  } else if (typeof textOrErrorObject.message === "string") {
    title = "Error";
    error = textOrErrorObject.message;
  } else {
    error = JSON.stringify(textOrErrorObject, null, 2);
  }

  if (error === "Network Error") {
    error = "API not reacheable - connection available?";
  }

  // okFun is the function passed to this Alert that contains the function executed
  // upon the Alerts box gets clicked away
  if (okFun) {
    title = "Hi!";
  }

  Alert.alert(
    title,
    error,
    [
      {
        text: "OK",
        onPress: okFun
      }
    ],
    { cancelable: false }
  );
};
