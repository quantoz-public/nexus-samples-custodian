# Nexus Samples - Custodian

This sample consists of
1. A webserver hosting an API to connect the Mobile Application and Nexus  
  [README](./CustodianSampleApi/README.md)
2. A Mobile Application  
  [README](./SampleApp/README.md)

## Disclaimer with the Nexus Custodian sample application

The Nexus sample application for custodian business models is an open source combination of a C# backend API providing a connection to the React-Native mobile phone application for end-customers. The API is connected with the Nexus API to handle creation accounts and transactions.

The end-customer application supports the following actions
- Create a customer with a custodian account
- Get an overview of the account status and transactions
- Create a payment to another account
  - Scan another account of another mobile phone app user using the QR code option

For the funding of accounts, the Nexus Portal can be used.

### MIT License
The sample application as-is should not be for production purposes, but as a proof-of-concept example or base for further or own development. Some changes are required to turn the sample application into a minimal viable and safe production product. On top of that you can develop all kind of additional customizations.

### Minimal adaptation for production
The sample application support the necessary options to implement a custodian based system for end-customers. The API is needed for the Nexus connection and will contain also the few customer details gathered during Mobile App Sign-up.

### Security - *IMPORTANT*
Use a different OAUTH server, to make this sample work, we use password grant, therefore the password will be send unencrypted to the authentication server, which is not adviced to use for a production system.

### Further customization options
1. The database is a simple local Sqlite db, which can easily be changed to something else.
1. The backend API also acts as the OAUTH authorization server, but the mobile app can be configured to connect to another OAUTH server.
1. Funding of the custodian accounts needs to be done manually using the Nexus Portal, it needs some additional effort to build in a "Top-Up" possibility, but a "Top Up" screen is added in the app as a mock-up for demonstration purposes.

The Quantoz Nexus team can help you with advice and support.
