﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustodianSampleApi.Data.Pocos
{
    public class Account
    {
        public string Address { get; set; }
        public string Name { get; set; }
    }
}
