﻿using System.Collections.Generic;

namespace CustodianSampleApi.Data.DTOs.Logic
{
    public class BalanceDTO
    {
        public IEnumerable<CurrencyBalanceDTO> CurrencyBalances { get; set; }
        public IEnumerable<CryptoBalancesDTO> CryptoBalances { get; set; }
    }

    public class CurrencyBalanceDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Total { get; set; }
    }

    public class CryptoBalancesDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Total { get; set; }
    }
}
