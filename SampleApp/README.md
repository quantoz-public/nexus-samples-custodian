# Connect to Proxy API

If you run the `Sample App`, you need to run the `CustodianSampleApi` project first. See the [README](../CustodianSampleApi/README.md) file of the project to see how to run it locally.
# React-Native WITHOUT Expo
## To debug in an Emulator
1. Start emulator using Android Studio
2. Start metro server in separate console window
	- yarn start
3. Build and publish to Emulator:
	- yarn android

It is recommended to use git(linux) bash to run/build the app.

# Reset environment after Android red screen "Unable to resolve module x"
#	or "Super expression must either be null or a function"
reset whole environment
1. git clean -dfx
2. rm -rf node_modules && yarn
3. rm -v node_modules/*/.babelrc
4. cd android && ./gradlew clean
5. cd .. && rm -rf $TMPDIR/haste-map*
6. yarn android

## !! You probably have to run this as the last step !!
7. Stop the Metro server: close Metro server window
7. rm -rf $TMPDIR/metro-cache/*
8. Start the Metro server: `yarn start` from root dir

## DEBUGGING
When strange errors happen when unrelated code is moved or removed, there is probably a javascript syntax
error somewhere. Commit all changes to automatically run the javascript linter tool. This might solve the problem.

# Extra for iOS
1. cd ios && pod repo update && pod install
2. yarn ios


- Often you have to *rebuild* a lot of times: `yarn android` before the build actually succeeds.
	 	Sometimes it helps when you delete the `android/app/build` folder first

At once, not really clear why, the build succeeds and loads to the emulator

# Running tests
`yarn test`
only specific tests:
`yarn test LoginScreen`

# Developing Android
1. `react-native start`
2. `react-native log-android`
3. `yarn android`

# Packaging
1. Publish the Custodian Sample Api
2. Uopdate SAMPLE_API and issuer in Config.js
3. Create a keystore file
	```
	cd android/app
	keytool -genkey -v -keystore my-release-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
	```
4. Configure the password in `android/gradle.properties`
5. Generate the release APK:
	cd android && ./gradlew assembleRelease
6. Find it at `nexus-samples-custodian\SampleApp\android\app\build\outputs\apk\release`
