﻿using System;

namespace CustodianSampleApi.Data.Pocos
{
    public class TransactionDto
    {
        public DateTime Created { get; private set; }
        public Account From { get; set; }
        public Account To { get; set; }
        public decimal Amount { get; private set; }
        public Token Token { get; private set; }
    }
}
