﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;
using CustodianSampleApi.Logic.Interfaces;
using static IdentityServer4.IdentityServerConstants;

namespace CustodianSampleApi.Controllers
{
    [Authorize(LocalApi.PolicyName)]
    [Route("api/[controller]")]
    [ApiController]
    public class BalanceController : BaseController
    {
        private readonly string _acceptedAnonymousRequestToken;
        private readonly IBalanceLogic _balanceLogic;

        public BalanceController(
            IConfiguration configuration,
            IBalanceLogic balanceLogic)
            : base()
        {
            _acceptedAnonymousRequestToken = configuration.GetValue<string>("accepted-anonymous-request-token");
            _balanceLogic = balanceLogic;
        }

        [HttpGet("customer/accounts/{customerCode}")]
        public async Task<ActionResult<BalanceDTO>> GetBalances([FromRoute] string customerCode)
        {
            var result = await _balanceLogic.GetBalance(customerCode);

            if (result == null)
            {
                return BadRequest("Customer balance could not be found or null");
            }

            return Ok(result);
        }
    }
}