import React, { useState, useEffect } from "react";
import { StyleSheet, Animated, Easing } from "react-native";
import { ListItem, Left, Icon, Button, Body, Text, Right } from "native-base";
import themeStyling from "./native-base-theme/variables/platform";

const styles = StyleSheet.create({
  listItem: {
    backgroundColor: "#fff",
    paddingRight: 0,
    height: 72
  },
  noLeftMargin: {
    marginLeft: 0
  },
  icon__outgoing: {
    color: themeStyling.whiteColor
  },
  icon__incoming: {
    color: themeStyling.nileBlue
  },
  iconContainer: {
    width: 40,
    height: 40
  },
  iconContainer__incoming: {
    backgroundColor: themeStyling.surfCrestColor
  },
  iconContainer__outgoing: {
    backgroundColor: themeStyling.curiousBlue
  }
});

const addDecimals = num => {
  return num.toFixed(Math.max(((num + "").split(".")[1] || "").length, 2));
};

const TransactionListItem = props => {
  // "buy", "sell" or "swap"
  const _operationType = props.transactionType.toLowerCase();
  const _isCryptoCurrency = props.currencyType === "crypto";
  const _isSwapSource =
    _operationType === "swap" && props.direction === "SOURCE";
  const _isSwapDestination =
    _operationType === "swap" && props.direction === "DESTINATION";

  const capitalizeFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  const _transactionIcon = (
    <Button
      style={[
        styles.iconContainer,
        _isCryptoCurrency
          ? _operationType === "sell"
            ? styles.iconContainer__outgoing
            : styles.iconContainer__incoming
          : _operationType === "sell"
          ? styles.iconContainer__incoming
          : styles.iconContainer__outgoing
      ]}
    >
      {_isCryptoCurrency ? (
        _operationType === "buy" ? (
          <Icon name="trending-up" style={styles.icon__incoming} />
        ) : _operationType === "sell" ? (
          <Icon name="trending-down" style={styles.icon__outgoing} />
        ) : (
          <Icon name="repeat" style={styles.icon__incoming} />
        )
      ) : _operationType === "buy" ? (
        <Icon name="trending-down" style={styles.icon__outgoing} />
      ) : _operationType === "sell" ? (
        <Icon name="trending-up" style={styles.icon__incoming} />
      ) : (
        <Icon name="repeat" style={styles.icon__incoming} />
      )}
    </Button>
  );

  return (
    <ListItem noIndent icon style={styles.listItem}>
      <Left>{_transactionIcon}</Left>
      <Body>
        {!_isCryptoCurrency ? (
          <Text body-bold-light>
            {capitalizeFirstLetter(props.code + " " + _operationType)}
          </Text>
        ) : (
          <Text body-bold-light>{capitalizeFirstLetter(_operationType)}</Text>
        )}
        <Text note>{props.date}</Text>
      </Body>
      <Right
        multilines={_isCryptoCurrency && _operationType !== "swap"}
        verticalCenter={!_isCryptoCurrency || _operationType === "swap"}
      >
        <Text
          body-bold-light
          incomingAmount={
            _isCryptoCurrency
              ? _operationType === "buy" || _isSwapDestination
              : _operationType === "sell" || _isSwapDestination
          }
        >
          {_isCryptoCurrency
            ? _operationType === "buy" || _isSwapDestination
              ? "+"
              : "-"
            : _operationType === "sell" || _isSwapDestination
            ? "+"
            : "-"}
          {_isCryptoCurrency
            ? addDecimals(Math.abs(props.amount.toFixed(5)))
            : addDecimals(Math.abs(props.amount.toFixed(2)))}
        </Text>
        {_isCryptoCurrency && _operationType !== "swap" ? (
          <Text note>
            EUR {addDecimals(Math.abs(props.fiatAmount.toFixed(2)))}
          </Text>
        ) : null}
      </Right>
    </ListItem>
  );
};

export default TransactionListItem;
