import React, { useState } from "react";
import { Text, Item, Input } from "native-base";
import { Grid, Col } from "react-native-easy-grid";
import { StyleSheet } from "react-native";
import { withNavigation } from "react-navigation";

const styles = StyleSheet.create({
  balanceAmount: {
    paddingTop: 4
  }
});

const handleSelectCrypto = props => {
  props.navigation.navigate("CryptoPickerScreen", {
    user: props.user,
    customerCode: props.navigation.getParam("customerCode"),
    sourceCurrencyCode: props.sourceCurrencyCode,
    setChosenSwapCrypto: props.setChosenSwapCrypto
  });
};

const CurrencyWidget = props => {
  const [value, setValue] = useState("");

  const handleInputChange = amount => {
    setValue(amount);
    props.setInputAmount(amount);
  };

  let cryptoName = <Text transaction>{props.currencyName}</Text>;

  if (props.isSwapOperation) {
    cryptoName = (
      <Text transaction onPress={() => handleSelectCrypto(props)}>
        {props.currencyName + " ▼"}
      </Text>
    );
  }

  return (
    <Grid>
      <Col>
        {cryptoName}
        <Text
          label-light
          style={[
            styles.balanceAmount,
            {
              color:
                props.isTransactionSource && value > props.balance
                  ? "red"
                  : null
            }
          ]}
        >
          Balance: {props.currencyCode}{" "}
          {props.currencyCode === "EUR"
            ? props.balance.toFixed(2)
            : props.balance === "-"
            ? props.balance
            : props.balance.toFixed(5)}
        </Text>
      </Col>
      <Col style={{ alignItems: "flex-end", justifyContent: "flex-start" }}>
        <Item regular>
          <Input
            transactionAmount
            keyboardType="numeric"
            placeholder="0"
            editable={!props.isTransactionDestination}
            autoFocus={props.isTransactionSource}
            value={
              props.isTransactionDestination
                ? String(
                    props.currencyCode === "EUR"
                      ? props.inputAmount === "-"
                        ? props.inputAmount
                        : props.inputAmount.toFixed(2)
                      : props.inputAmount === "-"
                      ? props.inputAmount
                      : props.inputAmount.toFixed(5)
                  )
                : value
            }
            style={[
              {
                color:
                  props.isTransactionSource && value > props.balance
                    ? "red"
                    : "#183C4E"
              }
            ]}
            onChangeText={amount => handleInputChange(amount)}
          />
        </Item>
      </Col>
    </Grid>
  );
};

export default withNavigation(CurrencyWidget);
