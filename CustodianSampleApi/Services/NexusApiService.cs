﻿using IdentityModel.Client;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CustodianSampleApi.Data.DTOs.Logic;
using CustodianSampleApi.Data.DTOs.Logic.Customer;
using CustodianSampleApi.Data.DTOs.Logic.Transaction;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService.Account;
using CustodianSampleApi.Data.DTOs.Services.NexusApiService.Customer;

namespace CustodianSampleApi.Services
{
    public class NexusApiService : INexusApiService
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpClientFactory _clientFactory;

        private readonly string _identityUrl;
        private readonly string _identityId;
        private readonly string _identitySecret;

        private string _token = null;
        private DateTime? _expirationTime = null;
        private string _refreshToken = null;

        public NexusApiService(IConfiguration configuration, IHttpClientFactory clientFactory)
        {
            _configuration = configuration;
            _clientFactory = clientFactory;

            _identityUrl = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-url");
            _identityId = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-id");
            _identitySecret = _configuration.GetSection("Nexus").GetValue<string>("nexus-identity-secret");
        }

        private async Task Register()
        {
            var client = _clientFactory.CreateClient();

            var response = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = $"{_identityUrl}/connect/token",

                ClientId = _identityId,
                ClientSecret = _identitySecret,
                Scope = "api1"
            });

            if (response.IsError) throw new Exception(response.Error);

            _token = response.AccessToken;
            // ExpiresIn - 10% safety margin
            _expirationTime = DateTime.UtcNow.AddSeconds((response.ExpiresIn - (response.ExpiresIn / 10)));
            _refreshToken = response.RefreshToken;
        }

        private async Task Refresh()
        {
            if (string.IsNullOrWhiteSpace(_refreshToken))
            {
                await Register();
            }
            else
            {
                var client = _clientFactory.CreateClient();

                var response = await client.RequestRefreshTokenAsync(new RefreshTokenRequest
                {
                    Address = $"{_identityUrl}/connect/token",

                    ClientId = _identityId,
                    ClientSecret = _identitySecret,

                    RefreshToken = _refreshToken
                });

                if (response.IsError) throw new Exception(response.Error);

                _token = response.AccessToken;
                _expirationTime = DateTime.UtcNow.AddSeconds(response.ExpiresIn);
                _refreshToken = response.RefreshToken;
            }
        }

        private async Task<HttpClient> GetClient()
        {
            if (_token == null)
            {
                await Register();
            }
            else if (_expirationTime != null && _expirationTime.Value < DateTime.UtcNow)
            {
                await Refresh();
            }

            var client = _clientFactory.CreateClient("NexusApiClient");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            client.DefaultRequestHeaders.Add("api_version", "1.2");

            return client;
        }

        private async Task<HttpResponseMessage> GetAsync(string endPoint)
        {
            var client = await GetClient();

            return await client.GetAsync(endPoint);
        }

        private class JsonContent : StringContent
        {
            public JsonContent(object obj) : base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json") { }
        }

        private async static Task<T> ReadContent<T>(HttpResponseMessage response)
        {
            var stringContent = await response.Content.ReadAsStringAsync();

            var content = JsonConvert.DeserializeObject<CustomResultHolder<T>>(stringContent);

            if (response.IsSuccessStatusCode)
            {
                return content.Values;
            }
            else
            {
                var inner = new HttpRequestException(string.Join("\n", content.Errors));
                throw new Exception($"{(int)response.StatusCode}", inner);
            }
        }

        private async Task<HttpResponseMessage> PostJsonAsync(string endPoint, object obj)
        {
            var client = await GetClient();

            return await client.PostAsync(endPoint, new JsonContent(obj));
        }

        public async Task<GetCustomerResponse> GetCustomer(string customerCode)
        {
            var endPoint = $"customer/{customerCode}";

            var encodedEndpoint = System.Web.HttpUtility.UrlEncode(endPoint);

            var result = await GetAsync(encodedEndpoint);

            return result.IsSuccessStatusCode ? await ReadContent<GetCustomerResponse>(result) : null;
        }

        public async Task<PostCustomerResponse> CreateCustomer(string customerCode, string email)
        {
            var customerDto = new PostCustomerRequest(customerCode, email);

            var endPoint = "customer";

            var result = await PostJsonAsync(endPoint, customerDto);

            return await ReadContent<PostCustomerResponse>(result);
        }

        public async Task<GetAccountResponse> GetAccount(string accountCode)
        {
            var endPoint = $"accounts/{accountCode}";

            var result = await GetAsync(endPoint);

            return await ReadContent<GetAccountResponse>(result);
        }

        public async Task<PostAccountResponse> CreateAccount(string customerCode)
        {
            var accountDto = new PostAccountRequest();

            var endPoint = $"customer/{customerCode}/accounts";

            var result = await PostJsonAsync(endPoint, accountDto);

            return await ReadContent<PostAccountResponse>(result);
        }

        public async Task<BalanceDTO> GetBalance(string customerCode)
        {
            var endPoint = $"balances/customers/{customerCode}";

            var result = await GetAsync(endPoint);

            return await ReadContent<BalanceDTO>(result);
        }
        public async Task<CustomerAccountsDTO> GetCustomerAccounts(string customerCode)
        {
            var endPoint = $"customer/{customerCode}/accounts";

            var result = await GetAsync(endPoint);

            return await ReadContent<CustomerAccountsDTO>(result);
        }
        public async Task<GetPrices> GetPrice(string currencyCode)
        {
            var endPoint = $"prices/{currencyCode}";

            var result = await GetAsync(endPoint);

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<GetPrices>();
        }

        public async Task<List<GetTransactionDTO>> GetTransactions(string cryptoCode, string accountCode, string status, string currencyCode, string customerCode)
        {
            var endPoint = $"/transactions/custodian?cryptoCode={cryptoCode}&accountCode={accountCode}&status={status}&currencyCode={currencyCode}&customerCode={customerCode}";

            var result = await GetAsync(endPoint);

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").SelectToken("records").ToObject<List<GetTransactionDTO>>();
        }

        public async Task<TotalTransactionReponseDTO> CreateBuyTransaction(BuyTransactionDTO buyTransaction)
        {
            var endPoint = $"/buy/custodian";

            var result = await PostJsonAsync(endPoint, new
            {
                buyTransaction.CustomerCode,
                buyTransaction.FiatValue,
                buyTransaction.CryptoCode,
                buyTransaction.PaymentMethodCode,
                buyTransaction.CurrencyCode,
                buyTransaction.AccountCode
            });

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<TotalTransactionReponseDTO>();
        }

        public async Task<GetPrices> GetPrice(string currencyCode, string cryptoCode)
        {
            var endPoint = $"prices/{currencyCode}/{cryptoCode}";

            var result = await GetAsync(endPoint);

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<GetPrices>();
        }

        public async Task<TotalTransactionReponseDTO> CreateSellTransaction(SellTransactionDTO sellTransaction)
        {
            var endPoint = $"/sell/custodian";

            var result = await PostJsonAsync(endPoint, new
            {
                sellTransaction.CustomerCode,
                CryptoAmount = sellTransaction.FiatValue,
                sellTransaction.CryptoCode,
                sellTransaction.PaymentMethodCode,
                sellTransaction.CurrencyCode,
                sellTransaction.AccountCode
            });

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<TotalTransactionReponseDTO>();
        }

        public async Task<CustodianSwapResponseDTO> CreateSwapTransaction(SwapTransactionDTO swapTransaction)
        {
            var endPoint = $"/swap/custodian";

            var result = await PostJsonAsync(endPoint, new
            {
                swapTransaction.CustomerCode,
                swapTransaction.PaymentMethodCode,
                Source = new
                {
                    swapTransaction.Source.AccountCode,
                    swapTransaction.Source.CryptoCode,
                    swapTransaction.Source.CryptoAmount,
                    swapTransaction.Source.RequestedPrice
                },
                Destination = new
                {
                    swapTransaction.Destination.AccountCode,
                    swapTransaction.Destination.CryptoCode,
                    swapTransaction.Destination.RequestedPrice
                }
            });

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<CustodianSwapResponseDTO>();
        }

        public async Task<TotalTransactionReponseDTO> SimulateBuyTransaction(BuyTransactionDTO buyTransaction)
        {
            var endPoint = $"/buy/custodian/simulate";

            var result = await PostJsonAsync(endPoint, new
            {
                buyTransaction.CustomerCode,
                buyTransaction.FiatValue,
                buyTransaction.CryptoCode,
                buyTransaction.PaymentMethodCode,
                buyTransaction.CurrencyCode,
                buyTransaction.AccountCode
            });

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<TotalTransactionReponseDTO>();
        }

        public async Task<TotalTransactionReponseDTO> SimulateSellTransaction(SellTransactionDTO sellTransaction)
        {
            var endPoint = $"/sell/custodian/simulate";

            var result = await PostJsonAsync(endPoint, new
            {
                sellTransaction.CustomerCode,
                CryptoAmount = sellTransaction.FiatValue,
                sellTransaction.CryptoCode,
                sellTransaction.PaymentMethodCode,
                sellTransaction.CurrencyCode,
                sellTransaction.AccountCode
            });

            var json = await result.Content.ReadAsStringAsync();

            return JObject.Parse(json).SelectToken("values").ToObject<TotalTransactionReponseDTO>();
        }
    }
}
